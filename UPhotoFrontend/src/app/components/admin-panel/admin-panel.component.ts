import { Component, OnInit } from '@angular/core';
import {User} from "../../models/user";
import {AdminService} from "../../services/admin.service";

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {
  users: Partial<User[]>;

  constructor(private _adminService: AdminService) { }

  ngOnInit(): void {
    this.getUsersWithRoles();
  }

  getUsersWithRoles() {
    this._adminService.getUsersWithRoles().subscribe(users => {
      this.users = users;
    });
  }
}
