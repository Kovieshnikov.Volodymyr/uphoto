import { Component, OnInit } from '@angular/core';
import {Member} from "../../../models/member";
import {MembersService} from "../../../services/members.service";
import {Pagination} from "../../../models/pagination";
import {UserParams} from "../../../models/userParams";
import {User} from "../../../models/user";

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss']
})
export class MemberListComponent implements OnInit {
  members: Member[];
  pagination: Pagination;
  userParams: UserParams;
  user: User;

  constructor(private _memberService: MembersService) {
    this.userParams = _memberService.UserParams;
  }

  ngOnInit(): void {
    this.loadMembers();
  }

  loadMembers() {
    this._memberService.UserParams = this.userParams;
    this._memberService.getMembers(this.userParams).subscribe(paginatedResponse => {
      this.members = paginatedResponse.result;
      this.pagination = paginatedResponse.pagination;
    });
  }

  resetFilters() {
    this.userParams = this._memberService.resetUserParams();
    this.loadMembers();
  }

  changePage(event: any) {
    this.userParams.pageNumber = event.pageIndex + 1;
    this.userParams.pageSize = event.pageSize;
    this._memberService.UserParams = this.userParams;
    this.loadMembers();
  }
}
