import {Component, OnInit, ViewChild} from '@angular/core';
import {Member} from "../../models/member";
import {User} from "../../models/user";
import {AccountService} from "../../services/account.service";
import {MembersService} from "../../services/members.service";
import {ToastrService} from "ngx-toastr";
import {take} from "rxjs/operators";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  member: Member;
  user: User;

  constructor(private accountService: AccountService,
              private memberService: MembersService,
              private toastr: ToastrService) {
    this.accountService.currentUser$.pipe(take(1)).subscribe(u => this.user = u);
  }

  ngOnInit(): void {
    this.loadMember();
  }

  loadMember() {
    this.memberService.getMember(this.user.username).subscribe(m => this.member = m);
  }

  updateMember() {
    this.memberService.updateMember(this.member).subscribe(() => {
      this.toastr.success('Your profile has been updated successfully');
      this.editForm.reset(this.member);
    });
  }
}
