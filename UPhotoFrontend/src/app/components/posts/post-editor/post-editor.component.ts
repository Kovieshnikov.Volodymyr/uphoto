import {Component, OnInit} from '@angular/core';
import {PostsService} from "../../../services/posts.service";
import {Post} from "../../../models/post";
import {AccountService} from "../../../services/account.service";
import {MembersService} from "../../../services/members.service";
import {take} from "rxjs/operators";
import {User} from "../../../models/user";
import {Member} from "../../../models/member";

@Component({
  selector: 'app-post-editor',
  templateUrl: './post-editor.component.html',
  styleUrls: ['./post-editor.component.scss']
})
export class PostEditorComponent implements OnInit {
  user: User;
  member: Member;
  posts: Post[];
  selectedImage: File;
  description: string;
  imgUrl: any;
  readyToSend: boolean = false;

  constructor(private _postsService: PostsService,
              private _accountService: AccountService,
              private _membersService: MembersService) {
    this._accountService.currentUser$.pipe(take(1)).subscribe(u => this.user = u);
  }

  ngOnInit(): void {
    this.loadMember();
    this.loadPosts();
  }

  loadPosts() {
    this._postsService.getUserPosts().subscribe(posts => this.posts = posts);
  }

  loadMember() {
    this._membersService.getMember(this.user.username).subscribe(m => this.member = m);
  }

  changeFile(event) {
    this.selectedImage = event.target.files[0];
    this.readyToSend = true;

    // const mimeType = this.selectedImage.type;
    // if (mimeType.match(/image\/*/) == null) {
    //   // this.msg = "Only images are supported";
    //   return;
    // }

    const fileReader = new FileReader();
    fileReader.readAsDataURL(event.target.files[0]);

    fileReader.onload = () => {
      this.imgUrl = fileReader.result;
    }
  }

  uploadPost() {
    this._postsService.uploadPhoto(this.selectedImage, this.description || "").subscribe();
    this.selectedImage = null;
    this.description = null;
    this.imgUrl = null;
    this.readyToSend = false;
  }

  deletePost(postId: number) {
    this._postsService.deletePost(postId)
      .subscribe(() => this.posts = this.posts.filter(p => p.id !== postId));
  }

  setAvatar(post: Post) {
    this._postsService.setAvatar(post.id).subscribe(() =>{
      this.user.profilePhotoUrl = post.photoUrl;
      this._accountService.setCurrentUser(this.user);
      this.member.profilePhotoUrl = post.photoUrl;
    });
  }
}
