import {Component, OnInit, ViewChild} from '@angular/core';
import {PostsService} from "../../../services/posts.service";
import {Post} from "../../../models/post";
import {Pagination} from "../../../models/pagination";
import {PostParams} from "../../../models/postParams";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  @ViewChild('filterForm') filterForm: NgForm;
  posts: Post[];
  pagination: Pagination;
  postParams: PostParams;

  constructor(private _postsService: PostsService) {
    this.postParams = _postsService.PostParams;
  }

  ngOnInit(): void {
    this.loadAllPosts();
  }

  loadAllPosts() {
    this._postsService.PostParams = this.postParams;
    this._postsService.getAllPosts(this.postParams).subscribe(response => {
      this.posts = response.result;
      this.pagination = response.pagination;
    });
  }

  resetFilters() {
    this._postsService.resetParams();
    this.filterForm.reset();
    this.loadAllPosts();
  }

  likePost(post: Post) {
    this._postsService.likePost(post.id).subscribe(() => {
        this.loadAllPosts();
    })
  }

  changePage(event: any) {
    this.postParams.pageNumber = event.pageIndex + 1;
    this.postParams.pageSize = event.pageSize;
    this._postsService.PostParams = this.postParams;
    this.loadAllPosts();
  }
}
