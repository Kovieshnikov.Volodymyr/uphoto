import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NavComponent} from './components/nav/nav.component';
import {RegisterComponent} from './components/register/register.component';
import {MemberDetailComponent} from './components/members/member-detail/member-detail.component';
import {MemberListComponent} from './components/members/member-list/member-list.component';
import {ProfileComponent} from './components/profile/profile.component';
import {HomeComponent} from './components/home/home.component';
import {FooterComponent} from './components/footer/footer.component';
import {ToastrModule} from "ngx-toastr";
import {NotFoundComponent} from './components/errors/not-found/not-found.component';
import {ServerErrorComponent} from './components/errors/server-error/server-error.component';
import {ErrorInterceptor} from "./interceptors/error.interceptor";
import {JwtInterceptor} from "./interceptors/jwt.interceptor";
import {MemberCardComponent} from './components/members/member-card/member-card.component';
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {PostListComponent} from './components/posts/post-list/post-list.component';
import {PostCardComponent} from './components/posts/post-card/post-card.component';
import {PostEditorComponent} from "./components/posts/post-editor/post-editor.component";
import {TextInputComponent} from './components/forms/text-input/text-input.component';
import {MatPaginatorModule} from "@angular/material/paginator";
import {AdminPanelComponent} from './components/admin-panel/admin-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    RegisterComponent,
    MemberDetailComponent,
    MemberListComponent,
    ProfileComponent,
    HomeComponent,
    FooterComponent,
    NotFoundComponent,
    ServerErrorComponent,
    MemberCardComponent,
    PostEditorComponent,
    PostListComponent,
    PostCardComponent,
    TextInputComponent,
    AdminPanelComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
    MatCardModule,
    MatInputModule,
    ReactiveFormsModule,
    MatPaginatorModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
