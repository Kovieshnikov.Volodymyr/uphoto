import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from "./components/profile/profile.component";
import {MemberListComponent} from "./components/members/member-list/member-list.component";
import {MemberDetailComponent} from "./components/members/member-detail/member-detail.component";
import {HomeComponent} from "./components/home/home.component";
import {AuthGuard} from "./guards/auth.guard";
import {NotFoundComponent} from "./components/errors/not-found/not-found.component";
import {ServerErrorComponent} from "./components/errors/server-error/server-error.component";
import {PostListComponent} from "./components/posts/post-list/post-list.component";
import {PostEditorComponent} from "./components/posts/post-editor/post-editor.component";
import {AdminPanelComponent} from "./components/admin-panel/admin-panel.component";
import {AdminGuard} from "./guards/admin.guard";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {path: 'profile', component: ProfileComponent},
      {path: 'posts', component: PostListComponent},
      {path: 'members', component: MemberListComponent},
      {path: 'members/:username', component: MemberDetailComponent},
      {path: 'editor', component: PostEditorComponent},
      {path: 'admin', component: AdminPanelComponent, canActivate: [AdminGuard]},
    ]
  },
  {path: 'not-found', component: NotFoundComponent},
  {path: 'server-error', component: ServerErrorComponent},
  {path: '**', component: NotFoundComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
