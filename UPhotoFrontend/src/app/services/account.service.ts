import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ReplaySubject} from "rxjs";
import {User} from "../models/user";
import {map} from "rxjs/operators";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = environment.apiUrl;
  private currentUserSource = new ReplaySubject<User>(1);
  currentUser$ = this.currentUserSource.asObservable();

  constructor(private http: HttpClient) {
  }

  login(model: any) {
    return this.http.post(this.baseUrl + 'account/login', model)
      .pipe(map((user: User) => {
        if (user) {
          this.setCurrentUser(user);
        }
      }));
  }

  register(model: any) {
    return this.http.post(this.baseUrl + 'account/register', model)
      .pipe(map((user: User) => {
        if (user) {
          this.setCurrentUser(user);
        }
      }))
  }

  setCurrentUser(user: User) {
    if (user === null)
      return;

    user.roles = [];
    const roles = this.getTokenDecoded(user.token).role;
    Array.isArray(roles) ? user.roles = roles : user.roles.push(roles);
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUserSource.next(user);
  }

  logout() {
    localStorage.removeItem('user');
    this.currentUserSource.next(null);
  }

  private getTokenDecoded(token) {
    return JSON.parse(atob(token.split('.')[1]));
  }
}
