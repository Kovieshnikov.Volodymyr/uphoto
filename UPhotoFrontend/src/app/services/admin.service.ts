import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  apiUrl = environment.apiUrl;

  constructor(private _httpClient: HttpClient) { }

  getUsersWithRoles() {
    return this._httpClient.get<Partial<User[]>>(this.apiUrl + 'admin');
  }
}
