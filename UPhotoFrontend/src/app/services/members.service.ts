import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {Member} from "../models/member";
import {map} from "rxjs/operators";
import {UserParams} from "../models/userParams";
import {getPaginatedResult, getPaginationHeaders} from "../helpers/paginationHelper";

@Injectable({
  providedIn: 'root'
})
export class MembersService {
  apiUrl = environment.apiUrl;
  members: Member[] = [];
  memberCache = new Map();
  private _userParams: UserParams;

  constructor(private _httpClient: HttpClient) {
    this._userParams = new UserParams();
  }

  get UserParams() {
    return this._userParams;
  }

  set UserParams(value: UserParams) {
    this._userParams = value;
  }

  resetUserParams() {
    this._userParams = new UserParams();
    return this._userParams;
  }

  resetMemberCache() {
    this.memberCache = new Map();
  }

  getMembers(userParams: UserParams) {
    let response = this.memberCache.get(Object.values(userParams).join('-'));
    if (response)
      return of(response);

    let params = getPaginationHeaders(userParams.pageNumber, userParams.pageSize);
    params = params.append('orderBy', userParams.orderBy);

    return getPaginatedResult<Member[]>(this.apiUrl + 'users', params, this._httpClient)
      .pipe(map(response => {
        this.memberCache.set(Object.values(userParams).join('-'), response);
        return response;
      }));
  }

  getMember(username: string): Observable<Member> {
    const member = this.members.find(m => m.username === username);
    if (member !== undefined) {
      return of(member);
    }

    return this._httpClient.get<Member>(this.apiUrl + 'users/' + username);
  }

  updateMember(member: Member) {
    return this._httpClient.put(this.apiUrl + 'users', member)
      .pipe(map(() => {
        const index = this.members.indexOf(member);
        this.members[index] = member;
      }));
  }
}
