import {Injectable} from '@angular/core';
import {Post} from "../models/post";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {AccountService} from "./account.service";
import {PostParams} from "../models/postParams";
import {getPaginatedResult, getPaginationHeaders} from "../helpers/paginationHelper";

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  _apiUrl = environment.apiUrl;
  userPosts: Post[] = [];
  private _postParams: PostParams;

  constructor(private _httpClient: HttpClient,
              private _accountService: AccountService) {
    this._postParams = new PostParams();
  }

  get PostParams() {
    return this._postParams;
  }

  set PostParams(value: PostParams) {
    this._postParams = value;
  }

  resetParams() {
    this._postParams = new PostParams();
  }

  getAllPosts(postParams: PostParams) {
    let params = getPaginationHeaders(postParams.pageNumber, postParams.pageSize);

    if (postParams.authorUsername)
      params = params.append('authorUsername', postParams.authorUsername);

    return getPaginatedResult<Post[]>(this._apiUrl + 'posts', params, this._httpClient);
  }

  getUserPosts(): Observable<Post[]> {
    return this._httpClient.get<Post[]>(this._apiUrl + 'posts/user')
      .pipe(map(posts => {
        this.userPosts = posts;
        return posts;
      }));
  }

  likePost(postId: number) {
    return this._httpClient.post(this._apiUrl + 'posts/like?postid=' + postId, {});
  }

  uploadPhoto(image: File, description: string): Observable<Post> {
    const uploadData = new FormData();
    uploadData.append('file', image);
    uploadData.append('description', description);

    return this._httpClient.post<Post>(this._apiUrl + 'posts', uploadData)
      .pipe(map(post => {
        this.userPosts.push(post);
        return post;
      }));
  }

  setAvatar(postId: number) {
    return this._httpClient.put(this._apiUrl + 'posts/' + postId, {});
  }

  deletePost(postId: number) {
    return this._httpClient.delete(this._apiUrl + 'posts/' + postId);
  }
}
