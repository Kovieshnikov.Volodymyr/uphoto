﻿import {DisplayOrder} from "./displayOrder";

export class UserParams {
  pageNumber = 1;
  pageSize = 10;
  orderBy:DisplayOrder = DisplayOrder.Activity;
}
