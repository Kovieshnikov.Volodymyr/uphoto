﻿export interface User {
  username: string;
  token: string;
  profilePhotoUrl: string;
  fullName: string;
  roles: string[];
}
