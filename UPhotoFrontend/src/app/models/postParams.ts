﻿export class PostParams {
  pageNumber = 1;
  pageSize = 20;
  authorUsername: string;
}
