﻿export interface Post {
  id: number;
  description: string;
  photoUrl: string;
  created: Date;
  author: string;
  authorId: number;
  authorUsername: string;
  likes: number;
  likedByCurrentUser: boolean;
}
