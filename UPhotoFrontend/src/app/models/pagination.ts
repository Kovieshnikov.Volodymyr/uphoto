﻿export interface Pagination {
  currentPage: number;
  totalPages: number;
  pageSize: number;
  totalItemsCount: number;
}
