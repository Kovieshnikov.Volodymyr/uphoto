﻿export interface Member {
  id: number;
  name: string;
  surname: string;
  username: string;
  age: number;
  email: string;
  city: string;
  country: string;
  phone: string;
  profilePhotoUrl: string;
  aboutSelf: string;
  lastActive: Date;
}
