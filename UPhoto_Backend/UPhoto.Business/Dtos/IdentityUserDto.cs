﻿namespace UPhoto.Business.Dtos
{
    public class IdentityUserDto
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string ProfilePhotoUrl { get; set; }
        public string FullName { get; set; }
    }
}