﻿using System;

namespace UPhoto.Business.Dto
{
    public class PostDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string PhotoUrl { get; set; }
        public string Author { get; set; }
        public int AuthorId { get; set; }
        public string AuthorUsername { get; set; }
        public DateTime Created { get; set; }
        public int Likes { get; set; }
        public bool LikedByCurrentUser { get; set; }
    }
}