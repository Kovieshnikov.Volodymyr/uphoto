﻿using System.Collections.Generic;

namespace UPhoto.Business.Dtos
{
    public class UserRolesDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public ICollection<string> Roles { get; set; }
    }
}