﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UPhoto.Business.Dto
{
    public class UserRegisterDto
    {
        [Required] 
        [RegularExpression(@"^[a-zA-Z-\s]{1,40}$", ErrorMessage = "Please, enter a valid name")] 
        public string Name { get; set; }
        [Required] 
        [RegularExpression(@"^[a-zA-Z-\s]{1,40}$", ErrorMessage = "Please, enter a valid surname")] 
        public string Surname { get; set; }
        [Required] 
        [StringLength(14, MinimumLength = 4)]
        public string Username { get; set; }
        [Required] public DateTime DateOfBirth { get; set; }
        [Required] [EmailAddress] 
        public string Email { get; set; }
        [Required] 
        [RegularExpression(@"^[a-zA-Z-\s]{1,40}$", ErrorMessage = "Please, enter a valid city name")] 
        public string City { get; set; }
        [Required] 
        [RegularExpression(@"^[a-zA-Z-\s]{1,40}$", ErrorMessage = "Please, enter a valid country name")] 
        public string Country { get; set; }
        [Required] [Phone] 
        [MinLength(12, ErrorMessage = "Phone must be at least 12 characters")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Phone must contain digits only")]
        public string Phone { get; set; }
        public string ProfilePhotoUrl { get; set; }
        public string AboutSelf { get; set; }
        [Required] 
        [StringLength(8, MinimumLength = 4)]
        public string Password { get; set; }
    }
}