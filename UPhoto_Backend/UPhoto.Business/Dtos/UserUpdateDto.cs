﻿namespace UPhoto.Business.Dto
{
    public class UserUpdateDto
    {
        public string Username { get; set; }
        public string AboutSelf { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}