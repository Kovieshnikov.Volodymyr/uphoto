﻿using System;

namespace UPhoto.Business.Dto
{
    public class UserProfileDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string ProfilePhotoUrl { get; set; }
        public string AboutSelf { get; set; }
        public string Password { get; set; }
        public DateTime LastActive { get; set; }
    }
}