﻿using Microsoft.AspNetCore.Http;

namespace UPhoto.Business.Dto
{
    public class AddPostDto
    {
        public string Description { get; set; }
        public IFormFile File { get; set; }
    }
}