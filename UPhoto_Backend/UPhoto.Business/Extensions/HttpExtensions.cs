﻿using System.Text.Json;
using Microsoft.AspNetCore.Http;
using UPhoto.Business.Utils;

namespace UPhoto.Business.Extensions
{
    public static class HttpExtensions
    {
        public static void AddPaginationHeader(this HttpResponse response,
            int currentPage, int itemsPerPage, int totalItems, int totalPages)
        {
            var paginationHeader = new PaginationHeader(currentPage, totalPages, itemsPerPage, totalItems);
            var serializerOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            
            response.Headers.Add("Pagination", JsonSerializer.Serialize(paginationHeader, serializerOptions));
            response.Headers.Add("Access-Control-Expose-Headers", "Pagination");
        }
    }
}