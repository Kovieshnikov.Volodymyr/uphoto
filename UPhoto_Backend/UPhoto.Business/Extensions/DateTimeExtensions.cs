﻿using System;

namespace UPhoto.Business.Extensions
{
    public static class DateTimeExtensions
    {
        public static int CalculateAge(this DateTime dateOfBirth)
        {
            var today = DateTime.Today;
            var age = today.Year - dateOfBirth.Year;

            //In case user hasn't had birthday this year yet
            if (dateOfBirth.Date > today.AddYears(-age))
                --age;

            return age;
        }
    }
}