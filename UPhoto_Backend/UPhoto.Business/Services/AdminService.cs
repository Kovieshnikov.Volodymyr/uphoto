﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UPhoto.Business.Dtos;
using UPhoto.Business.Exceptions;
using UPhoto.Business.Interfaces;
using IdentityUser = UPhoto.Identity.Entities.IdentityUser;

namespace UPhoto.Business.Services
{
    /// <summary>
    /// Service for admin users
    /// </summary>
    public class AdminService : IAdminService
    {
        private readonly UserManager<IdentityUser> _userManager;

        public AdminService(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// Gets users and roles
        /// </summary>
        /// <returns>Enumerable of users and roles</returns>
        /// <exception cref="ApiException">Thrown when facing problems getting user roles from Identity DB</exception>
        public async Task<IEnumerable<UserRolesDto>> GetUsersWithRolesAsync()
        {
            var users = await _userManager.Users
                .Include(u => u.UserRoles)
                .ThenInclude(r => r.Role)
                .OrderBy(u => u.UserName)
                .Select(u => new UserRolesDto
                {
                    Id = u.Id,
                    Username = u.UserName,
                    Roles = u.UserRoles.Select(r => r.Role.Name).ToList()
                })
                .ToListAsync();

            if (users == null)
                throw new ApiException("Error retrieving users");
            
            return users;
        }
    }
}