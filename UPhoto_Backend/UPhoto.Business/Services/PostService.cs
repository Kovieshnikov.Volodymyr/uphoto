﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using UPhoto.Business.Dto;
using UPhoto.Business.Exceptions;
using UPhoto.Business.Interfaces;
using UPhoto.Business.Utils;
using UPhoto.Data.Entities;
using UPhoto.Data.Interfaces;

namespace UPhoto.Business.Services
{
    /// <summary>
    /// Provides service related to posts
    /// </summary>
    public class PostService : IPostService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly Cloudinary _cloudinary;

        public PostService(IUnitOfWork unitOfWork, IMapper mapper,
            IOptions<CloudinarySettings> config)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

            var account = new Account(
                config.Value.CloudName,
                config.Value.ApiKey,
                config.Value.ApiSecret
            );

            _cloudinary = new Cloudinary(account);
        }

        /// <summary>
        /// Likes or dislikes a defined post
        /// </summary>
        /// <param name="userId">id of user who likes/dislikes a post</param>
        /// <param name="postId">id of post to like/dislike</param>
        /// <returns>True if like was successfully saved</returns>
        /// <exception cref="ApiException">Thrown when like operation fails</exception>
        public async ValueTask<bool> LikePostAsync(int userId, int postId)
        {
            if (userId <= 0 || postId <= 0)
                throw new ApiException("Server error occured");
            
            var post = _unitOfWork.PostRepository.GetPostWithLikesAndUser(postId);
            if (post == null)
                throw new ApiException("Server error occured");
            
            var like = post.Likes.SingleOrDefault(l => l.UserId == userId && l.PostId == postId);

            if (like != null)
                post.Likes.Remove(like);
            else
                post.Likes.Add(new Like { PostId = postId, UserId = userId });

            return await _unitOfWork.SaveAsync() > 0;
        }

        /// <summary>
        /// Gets user data and user posts
        /// </summary>
        /// <param name="username">Username of the user who's posts are required</param>
        /// <returns>Required user posts</returns>
        /// <exception cref="ApiException">Thrown when troubles getting user or posts are faced</exception>
        public async Task<IEnumerable<PostDto>> GetUserWithPostsAsync(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ApiException("Username can not be empty");
            
            var user = await _unitOfWork.UserRepository.GetUserWithPostsByUsernameAsync(username);
            if (user == null)
                throw new ApiException("Problem getting posts");

            var postsDto = _mapper.Map<IEnumerable<PostDto>>(user.Posts);

            return postsDto;
        }

        /// <summary>
        /// Gets paged result of user/users, posts and post likes
        /// </summary>
        /// <param name="postParams">Pagination and post parameters</param>
        /// <returns>Paged result</returns>
        public async Task<PagedList<PostDto>> GetPostsWithUsersAndLikesAsync(PostParams postParams)
        {
            var posts = postParams.AuthorUsername == null 
                ? _unitOfWork.PostRepository.GetPostsWithUsersAndLikes() 
                : _unitOfWork.PostRepository.GetPostsByUsername(postParams.AuthorUsername);

            var postsDto = await PagedList<PostDto>
                .CreateListAsync(posts.ProjectTo<PostDto>(_mapper.ConfigurationProvider), postParams.PageNumber, postParams.PageSize);
            
            foreach (var postDto in postsDto)
            {
                if (posts.First(p => p.Id == postDto.Id).Likes.FirstOrDefault(l => l.UserId == postParams.UserId) != null)
                    postDto.LikedByCurrentUser = true;
            }

            return postsDto;
        }

        /// <summary>
        /// Uploads post photo and adds post data to db
        /// </summary>
        /// <param name="username">Username of user who uploads the post</param>
        /// <param name="addPostDto">Post file and supporting data</param>
        /// <returns>Uploaded post data</returns>
        /// <exception cref="ApiException">Thrown when upload operation fails</exception>
        public async Task<PostDto> AddPostAsync(string username, AddPostDto addPostDto)
        {
            var user = await _unitOfWork.UserRepository.GetUserWithPostsByUsernameAsync(username);
            var file = addPostDto.File;
            
            if (file.Length <= 0)
                throw new ApiException("Bad or broken file");

            if (!file.ContentType.StartsWith("image/"))
                throw new ApiException("Only image files are acceptable");

            var uploadResult = await UploadPhoto(file);

            var post = new Post
            {
                Photo = new Photo
                {
                    Url = uploadResult.SecureUrl.AbsoluteUri,
                    PublicId = uploadResult.PublicId
                },
                Description = addPostDto.Description == "undefined" ? string.Empty : addPostDto.Description,
                Created = DateTime.Now
            };

            user.Posts.Add(post);

            if (await _unitOfWork.SaveAsync() <= 0)
                throw new ApiException("Failed to add a post");

            return _mapper.Map<PostDto>(post);
        }

        /// <summary>
        /// Sets a photo as user avatar
        /// </summary>
        /// <param name="username">User who's avatar to be changed</param>
        /// <param name="postId">id of post from which photo should be set as avatar</param>
        /// <returns>True if DB was successfully updated</returns>
        public async ValueTask<bool> SetUserAvatarAsync(string username, int postId)
        {
            if (string.IsNullOrWhiteSpace(username) || postId <= 0)
                throw new ApiException("Problem setting the avatar");
            
            var photoUrl = _unitOfWork.PostRepository.GetUserPostById(username, postId).Photo.Url;

            var user = await _unitOfWork.UserRepository.GetUserWithProfileByUsername(username);

            user.UserProfile.ProfilePhotoUrl = photoUrl;

            _unitOfWork.UserRepository.Update(user);
            return await _unitOfWork.SaveAsync() > 0;
        }

        /// <summary>
        /// Deletes a specified post
        /// </summary>
        /// <param name="username">User who's post is to be deleted</param>
        /// <param name="postId">Id of the post to be deleted</param>
        /// <returns>True if changes were successfully saved to the DB</returns>
        /// <exception cref="ApiException">Thrown when deletion problems faced</exception>
        public async ValueTask<bool> DeletePostAsync(string username, int postId)
        {
            if (string.IsNullOrWhiteSpace(username) || postId <= 0)
                throw new ApiException("Problem deleting the post");
            
            var post = _unitOfWork.PostRepository.GetUserPostById(username, postId);

            if (post == null)
                throw new ApiException("Post not found");
            
            if (post.Photo.PublicId != null) 
                await DeletePhoto(post.Photo.PublicId);

            _unitOfWork.PostRepository.Delete(post);
            return await _unitOfWork.SaveAsync() > 0;
        }
        
        private async Task<ImageUploadResult> UploadPhoto(IFormFile file)
        {
            await using var stream = file.OpenReadStream();
            
            var uploadParams = new ImageUploadParams
            {
                File = new FileDescription(file.FileName, stream),
                Transformation = new Transformation()
                    .Height(500)
                    .Width(500)
                    .Crop("fill")
                    .Gravity("face")
            };

            var uploadResult = await _cloudinary.UploadAsync(uploadParams);

            if (uploadResult.Error != null)
                throw new ApiException(uploadResult.Error.Message);

            return uploadResult;
        }

        private async Task DeletePhoto(string publicId)
        {
            var deleteParams = new DeletionParams(publicId);
            var cloudResult = await _cloudinary.DestroyAsync(deleteParams);
            
            if (cloudResult.Error != null)
                throw new ApiException(cloudResult.Error.Message);
        }
    }
}