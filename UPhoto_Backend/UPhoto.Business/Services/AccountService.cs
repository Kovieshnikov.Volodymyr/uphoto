﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UPhoto.Business.Dtos;
using UPhoto.Business.Interfaces;
using UPhoto.Data.Interfaces;
using UPhoto.Identity.IdentityExceptions;
using UPhoto.Identity.Interfaces;
using IdentityUser = UPhoto.Identity.Entities.IdentityUser;

namespace UPhoto.Business.IdentityServices
{
    /// <summary>
    /// Provides service regarding user identity
    /// </summary>
    public class AccountService : IAccountService
    {
        private readonly ITokenService _tokenService;
        private readonly IUnitOfWork _unitOfWork;

        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountService(ITokenService tokenService, IUnitOfWork unitOfWork,
            UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _tokenService = tokenService;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        /// <summary>
        /// Registers a new user based on provided data 
        /// </summary>
        /// <param name="registerDto">Data required for registration</param>
        /// <returns>User data with token</returns>
        /// <exception cref="IdentityException">Thrown when registration operation is not successful</exception>
        public async Task<IdentityUserDto> RegisterUserAsync(RegisterDto registerDto)
        {
            if (registerDto == null)
                throw new IdentityException("Register data is missing");
            
            if (await UserExists(registerDto.Username))
                throw new IdentityException("Username already taken");

            var identityUser = new IdentityUser { UserName = registerDto.Username.Trim().ToLower() };

            var userResult = await _userManager.CreateAsync(identityUser, registerDto.Password);
            if (!userResult.Succeeded)
                throw new IdentityException(userResult.Errors.ToString());

            var roleResult = await _userManager.AddToRoleAsync(identityUser, "Member");
            if (!roleResult.Succeeded)
                throw new IdentityException(roleResult.Errors.ToString());
            
            var user = await _unitOfWork.UserRepository.GetUserWithProfileByUsername(registerDto.Username);
            
            return new IdentityUserDto
            {
                Username = identityUser.UserName,
                Token = await _tokenService.CreateToken(identityUser),
                FullName = $"{user?.Name} {user?.Surname}"
            };
        }

        /// <summary>
        /// Logs in a user based on provided data
        /// </summary>
        /// <param name="loginDto">Username and password</param>
        /// <returns>Logged-in user with token</returns>
        /// <exception cref="IdentityException">Thrown when login operation is not successful</exception>
        public async Task<IdentityUserDto> LoginUser(LoginDto loginDto)
        {
            if (loginDto == null)
                throw new IdentityException("Login data is missing");
            
            var identityUser = await _userManager.Users
                .SingleOrDefaultAsync(x => x.UserName == loginDto.Username);

            if (identityUser == null)
                throw new IdentityException("Invalid username");

            var loginResult = await _signInManager.CheckPasswordSignInAsync(identityUser, loginDto.Password, false);
            if (!loginResult.Succeeded)
                throw new IdentityException("Login attempt failed");
            
            var user = await _unitOfWork.UserRepository.GetUserWithProfileByUsername(loginDto.Username);
            if (user == null)
                throw new IdentityException("User not found");
            
            return new IdentityUserDto()
            {
                Username = identityUser.UserName,
                Token = await _tokenService.CreateToken(identityUser),
                ProfilePhotoUrl = user.UserProfile?.ProfilePhotoUrl,
                FullName = $"{user.Name} {user.Surname}"
            };
        }

        private async ValueTask<bool> UserExists(string username) =>
            await _userManager.Users.AnyAsync(u => u.UserName == username.Trim().ToLower());
    }
}