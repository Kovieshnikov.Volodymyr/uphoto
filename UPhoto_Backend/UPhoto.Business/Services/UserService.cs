﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using UPhoto.Business.Interfaces;
using UPhoto.Business.Dto;
using UPhoto.Business.Exceptions;
using UPhoto.Business.Utils;
using UPhoto.Data.Entities;
using UPhoto.Data.Interfaces;

namespace UPhoto.Business.Services
{
    /// <summary>
    /// Provides service regarding app users
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Adds new user to DB
        /// </summary>
        /// <param name="userRegisterDto">New user data</param>
        /// <returns>True upon success</returns>
        /// <exception cref="ApiException">Thrown when passed dto is null</exception>
        public async ValueTask<bool> AddUserAsync(UserRegisterDto userRegisterDto)
        {
            if (userRegisterDto == null)
                throw new ApiException("Problems registering new user");

            var user = _mapper.Map<User>(userRegisterDto);

            _unitOfWork.UserRepository.Add(user);

            return await _unitOfWork.SaveAsync() == 1;
        }

        /// <summary>
        /// Returns paged users 
        /// </summary>
        /// <param name="userParams">Pagination and sorting params</param>
        /// <returns>Paged list of users</returns>
        public async Task<PagedList<UserProfileDto>> GetAllUsersWithProfilesAsync(UserParams userParams)
        {
            var users = _unitOfWork.UserRepository.GetAll()
                .Where(u => u.UserName != userParams.CurrentUsername && u.UserName != "admin");

            users = userParams.OrderBy switch
            {
                DisplayOrder.Created => users.OrderByDescending(u => u.Created),
                _ => users.OrderByDescending(u => u.LastActive)
            };

            return await PagedList<UserProfileDto>
                .CreateListAsync(users.ProjectTo<UserProfileDto>(_mapper.ConfigurationProvider),
                    userParams.PageNumber, userParams.PageSize);
        }

        /// <summary>
        /// Gets user with profile
        /// </summary>
        /// <param name="username">User to look for</param>
        /// <returns>User with profile data</returns>
        /// <exception cref="ApiException">Thrown when failed to get the user</exception>
        public async Task<UserProfileDto> GetByUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ApiException("Problem retrieving the user");
            
            var user = await _unitOfWork.UserRepository.GetUserWithProfileByUsername(username);

            if (user == null)
                throw new ApiException("User not found");

            var userDto = _mapper.Map<UserProfileDto>(user);
            return userDto;
        }

        /// <summary>
        /// Updates user data
        /// </summary>
        /// <param name="userUpdateDto">Data to be updated</param>
        /// <returns>True if update was successful</returns>
        /// <exception cref="ApiException">Thrown if update process failed</exception>
        public async ValueTask<bool> UpdateUserAsync(UserUpdateDto userUpdateDto)
        {
            if (userUpdateDto == null)
                throw new ApiException("Problem with update data");
            
            var user = await _unitOfWork.UserRepository.GetUserWithProfileByUsername(userUpdateDto.Username);
            if (user == null)
                throw new ApiException("User not found");

            _mapper.Map(userUpdateDto, user);
            _unitOfWork.UserRepository.Update(user);

            return await _unitOfWork.SaveAsync() > 0;
        }
    }
}