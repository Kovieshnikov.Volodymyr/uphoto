﻿using System.Threading.Tasks;
using UPhoto.Business.Dtos;

namespace UPhoto.Business.Interfaces
{
    public interface IAccountService
    {
        Task<IdentityUserDto> RegisterUserAsync(RegisterDto registerDto);
        Task<IdentityUserDto> LoginUser(LoginDto loginDto);
    }
}