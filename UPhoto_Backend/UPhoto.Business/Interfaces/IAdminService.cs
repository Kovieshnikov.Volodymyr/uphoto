﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UPhoto.Business.Dtos;

namespace UPhoto.Business.Interfaces
{
    public interface IAdminService
    {
        Task<IEnumerable<UserRolesDto>> GetUsersWithRolesAsync();
    }
}