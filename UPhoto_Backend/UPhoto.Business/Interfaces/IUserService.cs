﻿using System.Threading.Tasks;
using UPhoto.Business.Dto;
using UPhoto.Business.Utils;

namespace UPhoto.Business.Interfaces
{
    public interface IUserService
    {
        ValueTask<bool> AddUserAsync(UserRegisterDto userRegisterDto);
        Task<PagedList<UserProfileDto>> GetAllUsersWithProfilesAsync(UserParams userParams);
        Task<UserProfileDto> GetByUsername(string username);
        ValueTask<bool> UpdateUserAsync(UserUpdateDto userUpdateDto);
    }
}