﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UPhoto.Business.Dto;
using UPhoto.Business.Utils;

namespace UPhoto.Business.Interfaces
{
    public interface IPostService
    {
        ValueTask<bool> LikePostAsync(int userId, int postId);
        Task<IEnumerable<PostDto>> GetUserWithPostsAsync(string username);
        Task<PagedList<PostDto>> GetPostsWithUsersAndLikesAsync(PostParams postParams);
        Task<PostDto> AddPostAsync(string username, AddPostDto addPostDto);
        ValueTask<bool> DeletePostAsync(string username, int postId);
        ValueTask<bool> SetUserAvatarAsync(string username, int postId);
    }
}