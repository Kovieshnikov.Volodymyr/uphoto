﻿using AutoMapper;
using UPhoto.Business.Dto;
using UPhoto.Business.Extensions;
using UPhoto.Data.Entities;

namespace UPhoto.Business.Utils
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserProfileDto>()
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.UserProfile.City))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.UserProfile.Country))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.UserProfile.Phone))
                .ForMember(dest => dest.ProfilePhotoUrl, opt => opt.MapFrom(src => src.UserProfile.ProfilePhotoUrl))
                .ForMember(dest => dest.Age, opt => opt.MapFrom(src => src.UserProfile.DateOfBirth.CalculateAge()))
                .ForMember(dest => dest.AboutSelf, opt => opt.MapFrom(src => src.UserProfile.AboutSelf))
                .ReverseMap();

            CreateMap<UserUpdateDto, User>()
                .ForPath(dest => dest.UserProfile.City, opts => opts.MapFrom(src => src.City))
                .ForPath(dest => dest.UserProfile.Country, opts => opts.MapFrom(src => src.Country))
                .ForPath(dest => dest.UserProfile.AboutSelf, opts => opts.MapFrom(src => src.AboutSelf));

            CreateMap<Post, PostDto>()
                .ForMember(dest => dest.PhotoUrl, opts => opts.MapFrom(src => src.Photo.Url))
                .ForMember(dest => dest.Author, opts => opts.MapFrom(src => $"{src.User.Name} {src.User.Surname}"))
                .ForMember(dest => dest.Likes, opts => opts.MapFrom(src => src.Likes.Count))
                .ForMember(dest => dest.AuthorId, opts => opts.MapFrom(src => src.User.Id))
                .ForMember(dest => dest.AuthorUsername, opts => opts.MapFrom(src => src.User.UserName));

            CreateMap<UserRegisterDto, User>()
                .ForPath(dest => dest.UserProfile.DateOfBirth, opt => opt.MapFrom(src => src.DateOfBirth))
                .ForPath(dest => dest.UserProfile.City, opt => opt.MapFrom(src => src.City))
                .ForPath(dest => dest.UserProfile.Country, opts => opts.MapFrom(src => src.Country))
                .ForPath(dest => dest.UserProfile.Phone, opts => opts.MapFrom(src => src.Phone));
        }
    }
}