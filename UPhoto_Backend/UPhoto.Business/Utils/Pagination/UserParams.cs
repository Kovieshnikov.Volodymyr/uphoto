﻿namespace UPhoto.Business.Utils
{
    public class UserParams : PaginationParams
    {
        public string CurrentUsername { get; set; }
        public DisplayOrder OrderBy { get; set; } = DisplayOrder.Activity;
    }
}