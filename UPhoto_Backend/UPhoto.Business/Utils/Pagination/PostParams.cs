﻿namespace UPhoto.Business.Utils
{
    public class PostParams : PaginationParams
    {
        public int UserId { get; set; }
        public string AuthorUsername { get; set; }
    }
}