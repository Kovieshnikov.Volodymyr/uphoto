﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace UPhoto.Business.Utils
{
    public class PagedList<T> : List<T>
    {
        public PagedList(IEnumerable<T> items, 
            int itemCount, int pageNumber, int pageSize)
        {
            CurrentPage = pageNumber;
            TotalPages = (int)Math.Ceiling(itemCount / (double)pageSize);
            PageSize = pageSize;
            TotalItemsItemCount = itemCount;
            AddRange(items);
        }
        
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalItemsItemCount { get; set; }

        public static async Task<PagedList<T>> CreateListAsync(IQueryable<T> source, int pageNumber, int pageSize)
        {
            var itemCount = await source.CountAsync();
            var items = await source
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return new PagedList<T>(items, itemCount, pageNumber, pageSize);
        }
    }
}