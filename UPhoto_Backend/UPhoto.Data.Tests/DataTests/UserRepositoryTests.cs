﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using UPhoto.Data.Repositories;

namespace UPhoto.Data.Tests.DataTests
{
    [TestFixture]
    public class AppUserRepositoryTests
    {
        private DbContextOptions<DataContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = DataTestHelper.GetDalTestDbOptions();
        }
        
        [Test]
        public void UserRepository_GetUserWithProfileByUsername_ReturnsProperUser()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var user = userRepository.GetUserWithProfileByUsername("captain").Result;
            
            Assert.AreEqual("Steven", user.Name);
        }
        
        [Test]
        public void UserRepository_GetUserWithProfileByUsername_ReturnsUserProfile()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);
            
            var user = userRepository.GetUserWithProfileByUsername("captain").Result;

            Assert.IsNotNull(user.UserProfile);
            Assert.AreEqual("washington", user.UserProfile.City);
        }
        [Test]
        public void UserRepository_GetUserWithProfileByUsername_ReturnsNullIfUserNotFound()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);
            
            var user = userRepository.GetUserWithProfileByUsername("test").Result;
            
            Assert.IsNull(user);
        }

        [Test]
        public void UserRepository_GetUserWithPostsByUsernameAsync_ReturnsProperUser()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var user = userRepository.GetUserWithPostsByUsernameAsync("ironman").Result;
            
            Assert.AreEqual("Tony", user.Name);
        }
        
        [Test]
        public void UserRepository_GetUserWithPostsByUsernameAsync_ReturnsUserWithPosts()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var userPosts = userRepository.GetUserWithPostsByUsernameAsync("ironman").Result.Posts;
            
            Assert.IsNotEmpty(userPosts);
            Assert.AreEqual(1, userPosts.Count);
        }
        
        [Test]
        public void UserRepository_GetUserWithPostsByUsernameAsync_ReturnsUserWithPhotos()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var photo = userRepository.GetUserWithPostsByUsernameAsync("ironman")
                .Result.Posts.First().Photo;
            
            Assert.AreEqual("test", photo.Url);
        }
        
        [Test]
        public void UserRepository_GetUserWithPostsByUsernameAsync_ReturnsUserWithLikes()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var userLikes = userRepository.GetUserWithPostsByUsernameAsync("batman")
                .Result.Posts.First().Likes;
            
            Assert.AreEqual(1, userLikes.Count);
        }
        
        [Test]
        public void UserRepository_GetUserWithPostsByUsernameAsync_ReturnsNullIfUserNotFound()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var user = userRepository.GetUserWithPostsByUsernameAsync("joker").Result;
            
            Assert.IsNull(user);
        }
    }
}