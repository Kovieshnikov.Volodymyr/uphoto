﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using UPhoto.Data.Entities;
using UPhoto.Data.Repositories;

namespace UPhoto.Data.Tests.DataTests
{
    [TestFixture]
    public class BaseRepositoryTests
    {
        private DbContextOptions<DataContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = DataTestHelper.GetDalTestDbOptions();
        }
        
        [Test]
        public void BaseRepository_GetAll_ReturnsAllValues()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var users = userRepository.GetAll().ToList();
            
            Assert.IsNotNull(users);
            Assert.AreEqual(3, users.Count);
        }

        [Test]
        public async Task BaseRepository_GetByIdAsync_ReturnsProperUser()
        {
            await using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var user = await userRepository.GetByIdAsync(2);
            
            Assert.AreEqual("Steven", user.Name);
        }

        [Test]
        public void BaseRepository_Add_AddsUser()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);
            var user = new User { Name = "TestUser", Surname = "TestSurname" };
            
            userRepository.Add(user);
            context.SaveChanges();
            var users = userRepository.GetAll().ToList();
            
            Assert.AreEqual(user.Name, users.Last().Name);
        }

        [Test]
        public void BaseRepository_Update_UpdatesEntity()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var user = userRepository.GetAll().Last();
            user.Name = "NewName";
            userRepository.Update(user);
            context.SaveChanges();
            var checkName = userRepository.GetAll().Last().Name;
            
            Assert.AreEqual("NewName", checkName);
        }

        [Test]
        public void BaseRepository_Delete_DeletesUser()
        {
            using var context = new DataContext(_options);
            var userRepository = new UserRepository(context);

            var user = userRepository.GetByIdAsync(1).Result;
            userRepository.Delete(user);
            context.SaveChanges();
            var userCount = userRepository.GetAll().Count();
            
            Assert.AreEqual(2, userCount);
        }
    }
}