﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using UPhoto.Data.Repositories;

namespace UPhoto.Data.Tests.DataTests
{
    [TestFixture]
    public class PostsRepositoryTests
    {
        private DbContextOptions<DataContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = DataTestHelper.GetDalTestDbOptions();
        }
        
        [Test]
        public void PostRepository_GetPostWithLikesAndUser_ReturnsPost()
        {
            using var context = new DataContext(_options);
            var postsRepository = new PostRepository(context);

            var post = postsRepository.GetPostWithLikesAndUser(1);
            
            Assert.IsNotNull(post);
        }

        [Test]
        public void PostRepository_GetPostWithLikesAndUser_ReturnsPostWithLikes()
        {
            using var context = new DataContext(_options);
            var postsRepository = new PostRepository(context);

            var post = postsRepository.GetPostWithLikesAndUser(1);
            
            Assert.IsNotEmpty(post.Likes);
        }
        
        [Test]
        public void PostRepository_GetPostWithLikesAndUser_ReturnsPostWithUser()
        {
            using var context = new DataContext(_options);
            var postsRepository = new PostRepository(context);

            var postAuthor = postsRepository.GetPostWithLikesAndUser(1).User;
            
            Assert.IsNotNull(postAuthor);
            Assert.AreEqual("batman", postAuthor.UserName);
        }
        
        [Test]
        public void PostRepository_GetPostWithLikesAndUser_ReturnsZeroLikes()
        {
            using var context = new DataContext(_options);
            var postsRepository = new PostRepository(context);

            var post = postsRepository.GetPostWithLikesAndUser(3);
            var likes = post.Likes;
            
            Assert.IsEmpty(likes);
        }
        
        [Test]
        public void PostRepository_GetPostsWithUsersAndLikesAsync_ReturnsPosts()
        {
            using var context = new DataContext(_options);
            var postsRepository = new PostRepository(context);

            var posts = postsRepository.GetPostsWithUsersAndLikes().ToList();
            
            Assert.IsNotNull(posts);
            Assert.AreEqual(3, posts.Count());
        }
        
        [Test]
        public void PostRepository_GetPostsWithUsersAndLikesAsync_ReturnsPostsWithCorrespondingUsers()
        {
            using var context = new DataContext(_options);
            var postsRepository = new PostRepository(context);

            var user = postsRepository.GetPostsWithUsersAndLikes().Last().User;
            
            Assert.IsNotNull(user);
            Assert.AreEqual("Tony", user.Name);
        }
        
        [Test]
        public void PostRepository_GetUserPostById_ReturnsCorrectPost()
        {
            using var context = new DataContext(_options);
            var postsRepository = new PostRepository(context);

            var post = postsRepository.GetUserPostById("ironman", 3);
            
            Assert.IsNotNull(post);
            Assert.AreEqual("test", post.Photo.Url);
        }
        
        [Test]
        public void PostRepository_GetUserPostById_DoesNotPullUserProfile()
        {
            using var context = new DataContext(_options);
            var postsRepository = new PostRepository(context);

            var post = postsRepository.GetUserPostById("ironman", 3);
            
            Assert.IsNull(post.User.UserProfile);
        }
    }
}