﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using UPhoto.Data.Entities;

namespace UPhoto.Data.Tests
{
    internal static class DataTestHelper
    {
        public static DbContextOptions<DataContext> GetDalTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            using (var context = new DataContext(options))
            {
                SeedData(context);
            }

            return options;
        }

        public static void SeedData(DataContext context)
        {
            context.Users.Add(
                new User
                {
                    Id = 1, UserName = "batman", Name = "Bruce", Surname = "Wayne", Email = "batman@dc.com",
                    UserProfile = new UserProfile
                        { Id = 1, UserId = 1, City = "new-york", Phone = "555-555-111" },
                    Likes = new List<Like> { new Like { UserId = 1, PostId = 1 }, new Like { UserId = 1, PostId = 2 } }
                });

            context.Users.Add(
                new User
                {
                    Id = 2, UserName = "captain", Name = "Steven", Surname = "Rogers", Email = "captain@marvel.com",
                    UserProfile = new UserProfile
                        { Id = 2, UserId = 2, City = "washington", Phone = "555-555-222" },
                });

            context.Users.Add(
                new User
                {
                    Id = 3, UserName = "ironman", Name = "Tony",
                    Surname = "Stark", Email = "iamironman@marvel.com",
                    UserProfile = new UserProfile
                        { Id = 3, UserId = 3, City = "mayami", Phone = "555-555-333" },
                });


            context.Posts.Add(new Post
            {
                Id = 1, UserId = 1, Created = new DateTime(2021, 08, 01),
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
            });
            context.Posts.Add(new Post
            {
                Id = 2, UserId = 2, Created = new DateTime(2021, 07, 16),
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
            });
            context.Posts.Add(new Post
            {
                Id = 3, UserId = 3, Created = new DateTime(2021, 06, 12),
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                Photo = new Photo { Url = "test" }
            });

            context.SaveChanges();
        }
    }
}