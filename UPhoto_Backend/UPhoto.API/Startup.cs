using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.OpenApi.Models;
using UPhoto.API.Extensions;
using UPhoto.API.Middleware;

namespace UPhoto.API
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors();
            
            services.AddCustomServices(_configuration);
            services.AddIdentityServices(_configuration);
            
            services.AddSwaggerGen(s =>
                s.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "UPhoto API",
                    Version = "v1",
                }));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ExceptionMiddleware>();
            
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(s =>
                {
                    s.SwaggerEndpoint("/swagger/v1/swagger.json", "UPhoto API");
                    s.RoutePrefix = string.Empty;
                });
            }

            app.UseHttpsRedirection();
            
            app.UseRouting();

            app.UseCors(policy =>
                policy.AllowAnyHeader().AllowAnyMethod().WithOrigins("http://localhost:4200"));

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
                endpoints.MapControllers());
        }
    }
}
