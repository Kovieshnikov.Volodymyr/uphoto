﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UPhoto.Business.Dtos;
using UPhoto.Business.Interfaces;

namespace UPhoto.API.Controllers
{
    /// <summary>
    /// Controller available for admins only
    /// Allows to monitor user roles
    /// </summary>
    [Authorize(Policy = "RequireAdminRole")]
    public class AdminController : BaseAPIController
    {
        private readonly IAdminService _adminService;
        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }
        
        /// <summary>
        /// Gets users along with their roles
        /// </summary>
        /// <returns>App users with roles</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserRolesDto>>> Get()
        {
            var result = await _adminService.GetUsersWithRolesAsync();

            return Ok(result);
        }
    }
}