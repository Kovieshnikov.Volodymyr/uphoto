﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UPhoto.Business.Dto;
using UPhoto.Business.Dtos;
using UPhoto.Business.Interfaces;

namespace UPhoto.API.Controllers
{
    public class AccountController : BaseAPIController
    {
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;

        public AccountController(IAccountService accountService, IUserService userService)
        {
            _accountService = accountService;
            _userService = userService;
        }

        /// <summary>
        /// Takes new potential user data and registers new user in application
        /// </summary>
        /// <param name="userRegisterDto">User registration data</param>
        /// <returns>User data along with JWT token</returns>
        [HttpPost("register")]
        public async Task<ActionResult<IdentityUserDto>> Register(UserRegisterDto userRegisterDto)
        {
            if (userRegisterDto == null)
                return BadRequest("Registration error occured");

            await _userService.AddUserAsync(userRegisterDto);

            var result = await _accountService.RegisterUserAsync(new RegisterDto
                { Username = userRegisterDto.Username, Password = userRegisterDto.Password });

            return Ok(result);
        }

        /// <summary>
        /// Gets authenticated user with the JWT token
        /// </summary>
        /// <param name="loginDto">Username and password</param>
        /// <returns>User with JWT token</returns>
        [HttpPost("login")]
        public async Task<ActionResult<IdentityUserDto>> Login(LoginDto loginDto)
        {
            var result = _accountService.LoginUser(loginDto);

            return Ok(await result);
        }
    }
}