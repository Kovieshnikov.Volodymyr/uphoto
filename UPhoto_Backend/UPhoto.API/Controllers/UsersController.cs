﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UPhoto.Business.Dto;
using UPhoto.Business.Extensions;
using UPhoto.Business.Interfaces;
using UPhoto.Business.Utils;

namespace UPhoto.API.Controllers
{
    /// <summary>
    /// Controller that accepts requests regarding users
    /// </summary>
    [Authorize]
    public class UsersController : BaseAPIController
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Gets paginated users
        /// </summary>
        /// <param name="userParams">Pagination and user params</param>
        /// <returns>Paginated users result, code 200</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserProfileDto>>> GetAll([FromQuery] UserParams userParams)
        {
            userParams.CurrentUsername = User.GetUsername();
            var users = await _userService.GetAllUsersWithProfilesAsync(userParams);

            Response.AddPaginationHeader(users.CurrentPage, users.PageSize, users.TotalItemsItemCount,
                users.TotalPages);

            return Ok(users);
        }
        
        /// <summary>
        /// Get certain user by username
        /// </summary>
        /// <param name="username">Specified username</param>
        /// <returns>User with profile, code 200</returns>
        [HttpGet("{username}")]
        public async Task<ActionResult<UserProfileDto>> Get(string username)
        {
            var user = await _userService.GetByUsername(username);

            return Ok(user);
        }

        /// <summary>
        /// Updates user profile info
        /// </summary>
        /// <param name="userUpdateDto">Data to update</param>
        /// <returns>On success returns no content 204</returns>
        [HttpPut]
        public async Task<ActionResult> Update(UserUpdateDto userUpdateDto)
        {
            var username = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (username == null)
                return BadRequest("Identity Error. Try re-logging");
            userUpdateDto.Username = username;

            var result = _userService.UpdateUserAsync(userUpdateDto);

            if (await result)
                return NoContent();

            return BadRequest("Failed to update the user");
        }
    }
}