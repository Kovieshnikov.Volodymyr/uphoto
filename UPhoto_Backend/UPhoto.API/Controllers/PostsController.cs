﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UPhoto.Business.Dto;
using UPhoto.Business.Extensions;
using UPhoto.Business.Interfaces;
using UPhoto.Business.Utils;

namespace UPhoto.API.Controllers
{
    /// <summary>
    /// Controller that accepts requests regarding user posts
    /// </summary>
    [Authorize]
    public class PostsController : BaseAPIController
    {
        private readonly IPostService _postService;

        public PostsController(IPostService postService)
        {
            _postService = postService;
        }

        /// <summary>
        /// Gets paginated posts from all users or a specified user
        /// </summary>
        /// <param name="postParams">Pagination and user params</param>
        /// <returns>Paginated posts</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostDto>>> GetPosts([FromQuery] PostParams postParams)
        {
            postParams.UserId = User.GetUserId();
            var posts = await _postService.GetPostsWithUsersAndLikesAsync(postParams);

            Response.AddPaginationHeader(posts.CurrentPage, posts.PageSize, posts.TotalItemsItemCount,
                posts.TotalPages);

            return Ok(posts);
        }

        /// <summary>
        /// Get posts for current user 
        /// </summary>
        /// <returns>Posts of currently logged-in user, code 200</returns>
        [HttpGet("user")]
        public async Task<ActionResult<IEnumerable<PostDto>>> GetUserPosts()
        {
            var username = User.GetUsername();

            var posts = await _postService.GetUserWithPostsAsync(username);

            return Ok(posts);
        }

        /// <summary>
        /// Upload new photo to service
        /// </summary>
        /// <param name="description">Photo description</param>
        /// <param name="file">File holding a photo</param>
        /// <returns>Created status code 201</returns>
        [HttpPost]
        public async Task<ActionResult> Add([FromForm] string description, IFormFile file)
        {
            var username = User.GetUsername();

            var dto = new AddPostDto { Description = description, File = file };

            var post = await _postService.AddPostAsync(username, dto);

            return CreatedAtAction("GetAll", "Users", new { username }, post);
        }

        /// <summary>
        /// Add like to a chosen post 
        /// </summary>
        /// <param name="postId">id of the chosen post</param>
        /// <returns>On success returns no content status code 204</returns>
        [HttpPost("like")]
        public async Task<ActionResult> Like([FromQuery] int postId)
        {
            if (postId <= 0)
                return BadRequest("Invalid post id");

            var userId = User.GetUserId();

            var result = await _postService.LikePostAsync(userId, postId);
            if (result)
                return NoContent();

            return BadRequest("Failed to like a post");
        }

        /// <summary>
        /// Sets chosen photo as user avatar
        /// </summary>
        /// <param name="postId">id of the chosen post</param>
        /// <returns>On success returns no content result(code 204)</returns>
        [HttpPut("{postId:int}")]
        public async Task<ActionResult> SetAsAvatar(int postId)
        {
            var username = User.GetUsername();

            var result = await _postService.SetUserAvatarAsync(username, postId);

            if (result)
                return NoContent();

            return BadRequest("Failed to set new avatar");
        }

        /// <summary>
        /// Delete chosen user photo
        /// </summary>
        /// <param name="postId">Id of the post holding a photo</param>
        /// <returns>On success returns no content result(code 204)</returns>
        [HttpDelete("{postId:int}")]
        public async Task<ActionResult> Delete(int postId)
        {
            if (postId <= 0)
                return BadRequest("Invalid post id");

            var username = User.GetUsername();

            var result = await _postService.DeletePostAsync(username, postId);

            if (result)
                return NoContent();

            return BadRequest("Failed to delete the post");
        }
    }
}