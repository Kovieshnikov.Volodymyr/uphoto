﻿using Microsoft.AspNetCore.Mvc;
using UPhoto.API.Filters;

namespace UPhoto.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ServiceFilter(typeof(LogActivityFilter))]
    [ApiException]
    public abstract class BaseAPIController : ControllerBase
    {
    }
}