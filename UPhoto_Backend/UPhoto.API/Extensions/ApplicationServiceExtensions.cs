﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UPhoto.API.Filters;
using UPhoto.Business.Interfaces;
using UPhoto.Business.Services;
using UPhoto.Business.Utils;
using UPhoto.Data;
using UPhoto.Data.Interfaces;

namespace UPhoto.API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<CloudinarySettings>(configuration.GetSection("CloudinarySettings"));
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPostService, PostService>();

            services.AddScoped<LogActivityFilter>();
            services.AddScoped<ApiExceptionAttribute>();
            
            services.AddAutoMapper(typeof(AutoMapperProfile));
            
            services.AddDbContext<DataContext>(options => 
                options.UseSqlServer(configuration.GetConnectionString("UPhotoDBConnection")));
            
            return services;
        }
    }
}