﻿using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using UPhoto.API.Errors;
using UPhoto.Identity.IdentityExceptions;

namespace UPhoto.API.Middleware
{
    /// <summary>
    /// In case exception is thrown by application in shall be
    /// intercepted here and held in a graceful manner.
    /// Must be placed at the top of middleware 
    /// </summary>
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;
        private readonly IHostEnvironment _environment;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger, IHostEnvironment environment)
        {
            _next = next;
            _logger = logger;
            _environment = environment;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (IdentityException exception)
            {
                _logger.LogError(exception, exception.Message);
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }
            catch (SystemException exception)
            {
                _logger.LogError(exception, exception.Message);
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                var response = _environment.IsDevelopment()
                    ? new ApiInternalError(context.Response.StatusCode, exception.Message, exception.StackTrace)
                    : new ApiInternalError(context.Response.StatusCode, "Internal server error");

                var options = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };

                var json = JsonSerializer.Serialize(response, options);

                await context.Response.WriteAsync(json);
            }
        }
    }
}