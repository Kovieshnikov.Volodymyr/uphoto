using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using UPhoto.Data;
using UPhoto.Identity.Data;
using IdentityRole = UPhoto.Identity.Entities.IdentityRole;
using IdentityUser = UPhoto.Identity.Entities.IdentityUser;

namespace UPhoto.API
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;

            try
            {
                var identityContext = services.GetRequiredService<IdentityContext>();
                var userManager = services.GetRequiredService<UserManager<IdentityUser>>();
                var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
                await identityContext.Database.MigrateAsync();
                await SeedIdentity.SeedIdentityUsers(userManager, roleManager);
                
                var appContext = services.GetRequiredService<DataContext>();
                await appContext.Database.MigrateAsync();
                await SeedData.SeedUsers(appContext);
            }
            catch (Exception exception)
            {
                var logger = services.GetRequiredService<ILogger<Program>>();
                logger.LogError(exception, "Error during migration occured");

                throw;
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
