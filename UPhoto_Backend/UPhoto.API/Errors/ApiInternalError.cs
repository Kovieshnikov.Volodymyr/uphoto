﻿namespace UPhoto.API.Errors
{
    /// <summary>
    /// In case a server error occurs returns a preconfigured light-weight object 
    /// </summary>
    public class ApiInternalError
    {
        public int StatusCode { get; }
        public string Message { get; }
        public string Details { get; }

        public ApiInternalError(int statusCode, string message = null, string details = null)
        {
            StatusCode = statusCode;
            Message = message;
            Details = details;
        }
    }
}