﻿using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using UPhoto.API.Errors;
using UPhoto.Business.Exceptions;

namespace UPhoto.API.Filters
{
    /// <summary>
    /// Gracefully intercepts API custom exceptions
    /// Logs actual exception via a built-in logger and informs client of "Internal server error" 
    /// </summary>
    public class ApiExceptionAttribute : ExceptionFilterAttribute
    {
        public override async Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.Exception is ApiException exception)
            {
                context.ExceptionHandled = true;
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                
                var logger = context.HttpContext.RequestServices.GetService<ILogger<ApiException>>();
                logger.LogError(exception, exception.Message);
                
                var response = new ApiInternalError(context.HttpContext.Response.StatusCode, exception.Message,
                    "Internal server error");

                var serializerOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };

                var json = JsonSerializer.Serialize(response, serializerOptions);

                await context.HttpContext.Response.WriteAsync(json);
            }
        }
    }
}