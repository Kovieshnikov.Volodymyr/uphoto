﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using UPhoto.Business.Extensions;
using UPhoto.Data.Interfaces;

namespace UPhoto.API.Filters
{
    /// <summary>
    /// Updates user's last active record field upon receiving requests from a registered user
    /// </summary>
    public class LogActivityFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var resultContext = await next();
            
            if (!resultContext.HttpContext.User.Identity.IsAuthenticated)
                return;

            var userId = resultContext.HttpContext.User.GetUserId();
            var unitOfWork = resultContext.HttpContext.RequestServices.GetService<IUnitOfWork>();
            var user = await unitOfWork.UserRepository.GetByIdAsync(userId);
            user.LastActive = DateTime.Now;
            await unitOfWork.SaveAsync();
        }
    }
}