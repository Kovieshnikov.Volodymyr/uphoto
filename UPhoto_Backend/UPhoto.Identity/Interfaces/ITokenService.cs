﻿using System.Threading.Tasks;
using UPhoto.Identity.Entities;

namespace UPhoto.Identity.Interfaces
{
    public interface ITokenService
    {
        Task<string> CreateToken(IdentityUser user);
    }
}