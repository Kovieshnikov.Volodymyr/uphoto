﻿using System;
using System.Runtime.Serialization;

namespace UPhoto.Identity.IdentityExceptions
{
    [Serializable]
    public class IdentityException : Exception
    {
        public IdentityException(string message) : base(message)
        {

        }

        protected IdentityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
    }
}