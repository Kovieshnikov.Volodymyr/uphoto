﻿using Microsoft.AspNetCore.Identity;

namespace UPhoto.Identity.Entities
{
    public class IdentityUserRole : IdentityUserRole<int>
    {
        public IdentityUser User { get; set; }
        public IdentityRole Role { get; set; }
    }
}