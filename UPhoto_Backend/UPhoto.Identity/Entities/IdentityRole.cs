﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace UPhoto.Identity.Entities
{
    public class IdentityRole : IdentityRole<int>
    {
        public ICollection<IdentityUserRole> UserRoles { get; set; }
    }
}