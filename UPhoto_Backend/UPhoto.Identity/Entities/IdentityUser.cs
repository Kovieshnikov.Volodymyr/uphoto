﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace UPhoto.Identity.Entities
{
    public class IdentityUser : IdentityUser<int>
    {
        public ICollection<IdentityUserRole> UserRoles { get; set; }
    }
}