﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using IdentityRole = UPhoto.Identity.Entities.IdentityRole;
using IdentityUser = UPhoto.Identity.Entities.IdentityUser;

namespace UPhoto.Identity.Data
{
    public static class SeedIdentity
    {
        public static async Task SeedIdentityUsers(UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            if (await userManager.Users.AnyAsync())
                return;

            var userData = await File.ReadAllTextAsync("../UPhoto.Identity/Data/id_generated.json");
            var users = JsonSerializer.Deserialize<List<IdentityUser>>(userData);

            var roles = new List<IdentityRole>
            {
                new IdentityRole { Name = "Member" },
                new IdentityRole { Name = "Admin" },
                new IdentityRole { Name = "Moderator" }
            };

            foreach (var role in roles)
                await roleManager.CreateAsync(role);

            foreach (var user in users)
            {
                user.UserName = user.UserName.Trim().ToLower();

                await userManager.CreateAsync(user, "Pa$$w0rd");
                await userManager.AddToRoleAsync(user, "Member");
            }

            var admin = new IdentityUser { UserName = "admin" };
            await userManager.CreateAsync(admin, "Pa$$w0rd");
            await userManager.AddToRolesAsync(admin, new[] { "Admin", "Moderator" });
        }
    }
}