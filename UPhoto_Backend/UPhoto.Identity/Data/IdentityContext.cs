﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using UPhoto.Identity.Entities;
using IdentityRole = UPhoto.Identity.Entities.IdentityRole;
using IdentityUser = UPhoto.Identity.Entities.IdentityUser;

namespace UPhoto.Identity.Data
{
    public class IdentityContext : IdentityDbContext<IdentityUser, IdentityRole, int, IdentityUserClaim<int>,
        IdentityUserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public IdentityContext(DbContextOptions<IdentityContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<IdentityUser>()
                .HasMany(iu => iu.UserRoles)
                .WithOne(r => r.User)
                .HasForeignKey(r => r.UserId)
                .IsRequired();

            builder.Entity<IdentityRole>()
                .HasMany(ir => ir.UserRoles)
                .WithOne(u => u.Role)
                .HasForeignKey(r => r.RoleId)
                .IsRequired();
        }
    }
}