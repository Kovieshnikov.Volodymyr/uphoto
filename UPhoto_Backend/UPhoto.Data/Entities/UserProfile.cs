﻿using System;

namespace UPhoto.Data.Entities
{
    public class UserProfile : BaseEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string ProfilePhotoUrl { get; set; }
        public string AboutSelf { get; set; }
    }
}