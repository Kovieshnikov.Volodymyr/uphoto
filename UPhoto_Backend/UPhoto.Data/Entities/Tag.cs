﻿using System.Collections.Generic;

namespace UPhoto.Data.Entities
{
    public class Tag : BaseEntity
    {
        public string TagTitle { get; set; }
        public ICollection<PostTag> PostsLink { get; set; }
    }
}