﻿using System;
using System.Collections.Generic;

namespace UPhoto.Data.Entities
{
    public class Post : BaseEntity
    {
        public User User { get; set; }
        public int UserId { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public DateTime Created { get; set; }
        public string Description { get; set; }
        public Photo Photo { get; set; }
        public ICollection<Like> Likes { get; set; }
        public ICollection<PostTag> TagsLink { get; set; }
        public bool SoftDeleted { get; set; }
    }
}