﻿namespace UPhoto.Data.Entities
{
    public class Photo : BaseEntity
    {
        public string Url { get; set; }
        public string PublicId { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}