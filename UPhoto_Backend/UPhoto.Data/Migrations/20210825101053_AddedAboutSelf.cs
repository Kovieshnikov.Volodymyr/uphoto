﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UPhoto.DAL.Migrations
{
    public partial class AddedAboutSelf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AboutSelf",
                table: "UserProfile",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AboutSelf",
                table: "UserProfile");
        }
    }
}
