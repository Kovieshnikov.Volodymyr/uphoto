﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UPhoto.Data.Entities;
using UPhoto.Data.Interfaces;

namespace UPhoto.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private DataContext DataContext => Context as DataContext;

        public UserRepository(DbContext context) : base(context)
        {
        }
        
        public async Task<User> GetUserWithProfileByUsername(string username) =>
            await DataContext.Users
                .Include(u => u.UserProfile)
                .SingleOrDefaultAsync(u => u.UserName == username);

        public async Task<User> GetUserWithPostsByUsernameAsync(string username) =>
            await DataContext.Users
                .Include(u => u.Posts)
                .ThenInclude(p => p.Photo)
                .Include(u => u.Posts)
                .ThenInclude(p => p.Likes)
                .SingleOrDefaultAsync(u => u.UserName == username);
    }
}