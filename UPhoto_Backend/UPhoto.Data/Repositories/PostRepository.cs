﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using UPhoto.Data.Entities;
using UPhoto.Data.Interfaces;

namespace UPhoto.Data.Repositories
{
    public class PostRepository : Repository<Post>, IPostRepository
    {
        private DataContext DataContext => Context as DataContext;

        public PostRepository(DbContext context) : base(context)
        {
        }

        public Post GetPostWithLikesAndUser(int postId) =>
            DataContext.Posts
                .Include(p => p.Likes)
                .ThenInclude(l => l.User)
                .SingleOrDefault(p => p.Id == postId);

        public IQueryable<Post> GetPostsWithUsersAndLikes() =>
            DataContext.Posts
                .Include(p => p.User)
                .Include(p => p.Likes)
                .Include(p => p.Photo)
                .AsNoTracking()
                .OrderByDescending(p => p.Created)
                .AsQueryable();

        public IQueryable<Post> GetPostsByUsername(string username) =>
            DataContext.Posts
                .Where(p => p.User.UserName == username)
                .Include(p => p.Photo)
                .Include(p => p.Likes);

        public Post GetUserPostById(string username, int postId) =>
            DataContext.Posts
                .Include(p => p.User)
                .Include(p => p.Photo)
                .Where(u => u.User.UserName == username)
                .SingleOrDefault(p => p.Id == postId);
    }
}