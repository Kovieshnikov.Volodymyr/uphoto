﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UPhoto.Data.Entities;

namespace UPhoto.Data
{
    public static class SeedData
    {
        public static async Task SeedUsers(DataContext context)
        {
            if (await context.Users.AnyAsync())
                return;

            var userData = await File.ReadAllTextAsync("../UPhoto.Data/Utils/generated.json");
            var users = JsonSerializer.Deserialize<List<User>>(userData);

            context.Users.AddRange(users);
            
            context.Users.Add(new User
            {
                UserName = "admin",
                Name = "Admin",
                Surname = "Adminoff",
            });
            
            await context.SaveChangesAsync();
        }
    }
}