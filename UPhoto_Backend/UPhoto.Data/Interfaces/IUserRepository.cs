﻿using System.Threading.Tasks;
using UPhoto.Data.Entities;

namespace UPhoto.Data.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetUserWithProfileByUsername(string username);
        Task<User> GetUserWithPostsByUsernameAsync(string username);
    }
}