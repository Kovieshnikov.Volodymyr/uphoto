﻿using System.Linq;
using System.Threading.Tasks;
using UPhoto.Data.Entities;

namespace UPhoto.Data.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IQueryable<TEntity> GetAll();

        Task<TEntity> GetByIdAsync(int id);
        
        void Add(TEntity entity);
        
        void Update(TEntity entity);
        
        void Delete(TEntity entity);
    }
}