﻿using System;
using System.Threading.Tasks;

namespace UPhoto.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }
        IPostRepository PostRepository { get; }
        
        Task<int> SaveAsync();
    }
}