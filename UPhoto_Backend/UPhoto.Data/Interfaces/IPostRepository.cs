﻿using System.Linq;
using UPhoto.Data.Entities;

namespace UPhoto.Data.Interfaces
{
    public interface IPostRepository : IRepository<Post>
    {
        Post GetPostWithLikesAndUser(int postId);
        IQueryable<Post> GetPostsWithUsersAndLikes();
        Post GetUserPostById(string username, int postId);
        IQueryable<Post> GetPostsByUsername(string username);
    }
}