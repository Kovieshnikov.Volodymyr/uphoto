﻿using System.Threading.Tasks;
using UPhoto.Data.Interfaces;
using UPhoto.Data.Repositories;

namespace UPhoto.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public IUserRepository UserRepository { get; }
        public IPostRepository PostRepository { get; }
        
        public UnitOfWork(DataContext context)
        {
            _context = context;
            UserRepository = new UserRepository(_context);
            PostRepository = new PostRepository(_context);
        }
        
        public Task<int> SaveAsync() => 
            _context.SaveChangesAsync();

        public void Dispose() => 
            _context.Dispose();
    }
}