﻿using Microsoft.EntityFrameworkCore;
using UPhoto.Data.Entities;

namespace UPhoto.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(u => u.UserName)
                .IsUnique();
            
            modelBuilder.Entity<PostTag>().HasKey(postTag => new { postTag.PostId, postTag.TagId });

            modelBuilder.Entity<Post>().HasQueryFilter(p => !p.SoftDeleted);

            modelBuilder.Entity<Comment>()
                .ToTable("Comments")
                .HasOne(c => c.User)
                .WithMany(u => u.Comments)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Like>()
                .ToTable("Likes")
                .HasOne(l => l.User)
                .WithMany(u => u.Likes)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Tag>()
                .ToTable("Tags");
        }
    }
}